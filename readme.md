#THE FELLOWSHIP OF THE GENERICS

##Servern för spelet är gjord i NodeJS med Express, och gränssnittet är vanlig html, css och javascript.

För att kunna spela behöver du installera NodeJS på din dator (https://nodejs.org/en/download/). Sedan är det bara att i din terminal gå in i mappen 'game-server' och skriva npm start. Sedan kan du surfa in på 'localhost'.

Om du använder windows kan du bara dubbelklicka på filen 'start-game.bat', så startar servern och en chrome-flik med webbapplikationen.