"use strict";

let fs = require('fs');
let path = require('path');
let categeories = require('../models/card').categories;


exports.createCard = function (card, callback) {

    let folder = path.join( __dirname, "../data/" + card.category);
    let files = fs.readdirSync(folder);
    let id = 0;

    console.log('length: ' + files.length)
    console.log('last file: ' + files[files.length - 1])
   
    while (files.indexOf(card.category + id) > -1) {
        id++;
    }
   
    console.log("id: " + id)

    fs.writeFile(folder + "/" + card.category + id, JSON.stringify(card), callback); 
}

exports.updateCard = function (card, callback) {

    let folder = path.join( __dirname, "../data/" + card.category);
    fs.unlinkSync(folder + "/" + card.id);
    fs.writeFile(folder + "/" + card.id, JSON.stringify(card), callback); 
}

exports.getAllCards = function () {

    let res = {};
    
    Object.keys(categeories).forEach((category) => {

        let dirname = path.resolve(__dirname, '../data/' + category);
        res[category] = [];

        fs.readdirSync(dirname).forEach((file) => {
            let c = JSON.parse(fs.readFileSync(dirname + '/' + file, "utf8"));
            c.id = file;
            res[category].push(c);
        });
    });

    return res;
}

exports.getCard = function(id) {

    let category = id.replace(/[0-9]/, '');

    let dirname = path.resolve(__dirname, '../data/' + category);

    let res = JSON.parse(fs.readFileSync(dirname + '/' + id, "utf8"));
    res.id = id;

    return res
}