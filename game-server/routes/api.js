var express = require('express');
var router = express.Router();
var cardController = require('../controllers/card-controller.js');


router.post('/card', cardController.create_card);

router.put('/card', cardController.update_card);

router.get('/cards', cardController.get_all_cards);

router.get('/card/:id', cardController.get_card);



module.exports = router;

